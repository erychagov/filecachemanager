﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Windows.Storage;
using FileCacheManager.Files;
using FileCacheManager.Network;
using SQLite.Net;

namespace FileCacheManager
{
    public class CacheManager : IDisposable
    {
        public event SetCookie SetCookieEvent;
        public delegate void SetCookie( FileDownloader downloader );

        private readonly string DB_NAME;

        private bool isInit;
        private Dictionary<string, FileInformation> fileCache;
        private SQLiteConnection dbConnection;
        private FileDownloader downloader;

        private const string EmptyDBName = "FileCacheManager: Database name can't be empty";
        private const string NotInitErrorMessage = "FileCacheManager: It should be initialized";

        public CacheManager( string dbName )
        {
            isInit = false;

            fileCache = null;
            dbConnection = null;
            downloader = new FileDownloader();

            if ( string.IsNullOrEmpty( dbName ) )
                throw new FormatException( EmptyDBName );

            DB_NAME = dbName;
        }

        /// <summary>
        /// Connect to DB, create table, load info to RAM
        /// </summary>
        /// <returns>Task</returns>
        public Task Initialization()
        {
            return Task.Factory.StartNew( () =>
            {
                lock ( this )
                {
                    if ( isInit )
                        return;

                    fileCache = new Dictionary<string, FileInformation>();

                    StorageFolder localFolder = ApplicationData.Current.LocalFolder;

                    //connection to DB
                    dbConnection = new SQLiteConnection( new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), System.IO.Path.Combine( localFolder.Path, DB_NAME ) );
                    dbConnection.CreateTable( typeof( FileInformation ) );

                    //read all data
                    List<FileInformation> allFilesInformation = dbConnection.Table<FileInformation>().ToList();
                    foreach ( FileInformation fileInformation in allFilesInformation )
                    {
                        if ( !fileCache.ContainsKey( fileInformation.Url ) )
                            fileCache.Add( fileInformation.Url, fileInformation );

                        //TODO: check url: file exist?
                        //TODO: if file cache already contains key, we can remove it from DB
                    }

                    var handler = SetCookieEvent;
                    if ( handler != null )
                        handler( downloader );

                    isInit = true;
                }
            } );
        }

        /// <summary>
        /// Close connection to DB, clear cache in RAM
        /// </summary>
        /// <returns>Task</returns>
        public void Close()
        {
            lock ( this )
            {
                if ( !isInit )
                    throw new InvalidOperationException( NotInitErrorMessage );

                dbConnection.Close();
                fileCache.Clear();

                dbConnection = null;
                fileCache = null;

                isInit = false;
            }
        }

        public void Dispose()
        {
            try
            {
                Close();
            }
            catch { }
        }

        /// <summary>
        /// Clear table in DB, remove all files, clear cache in RAM
        /// </summary>
        /// <returns>Task</returns>
        public Task ClearCache()
        {
            return Task.Factory.StartNew( () =>
            {
                lock ( this )
                {
                    if ( !isInit )
                        throw new InvalidOperationException( NotInitErrorMessage );

                    dbConnection.DeleteAll<FileInformation>();

                    //get file and remove it
                    foreach ( FileInformation fileInfo in fileCache.Values )
                    {
                        StorageFile file = StorageFile.GetFileFromPathAsync( fileInfo.LocalPath ).AsTask().Result;

                        file.DeleteAsync( StorageDeleteOption.PermanentDelete ).AsTask();
                    }

                    fileCache.Clear();
                }
            } );
        }

        /// <summary>
        /// Trying to find downloaded file by url
        /// </summary>
        /// <param name="url">Full url to file</param>
        /// <returns>Storage file or null if file have not been found</returns>
        public StorageFile GetStorageFile( string url )
        {
            lock ( this )
            {
                if ( !isInit )
                    throw new InvalidOperationException( NotInitErrorMessage );

                if ( !fileCache.ContainsKey( url ) )
                    return null;

                try
                {
                    FileInformation fileInfo = fileCache[url];
                    return StorageFile.GetFileFromPathAsync( fileInfo.LocalPath ).AsTask().Result;
                }
                catch ( Exception e )
                {
                    System.Diagnostics.Debug.WriteLine( e );
                    return null;
                }
            }
        }

        /// <summary>
        /// Download file by url
        /// </summary>
        /// <param name="url">Full url to file</param>
        /// <param name="fileName">File will be saved with this name or file name will generate without extension</param>
        /// <returns>Storage file or null if file have not been downloaded</returns>
        public Task<StorageFile> DownloadFile( string url, string fileName = "" )
        {
            return Task.Factory.StartNew( () =>
            {
                lock ( this )
                {
                    if ( !isInit )
                        throw new InvalidOperationException( NotInitErrorMessage );

                    StorageFile downloadedFile = downloader.DownloadFile( url, fileName );

                    if ( downloadedFile != null )
                    {
                        updateCache( new FileInformation( url, downloadedFile.Path ) );
                    }

                    return downloadedFile;
                }
            } );
        }

        /// <summary>
        /// Manually add file to cache
        /// </summary>
        /// <param name="url">Full url to file</param>
        /// <param name="file">Storage file in any folder</param>
        /// <returns>Task</returns>
        public Task AddStorageFileToCache( string url, StorageFile file )
        {
            return Task.Factory.StartNew( () =>
            {
                lock ( this )
                {
                    if ( !isInit )
                        throw new InvalidOperationException( NotInitErrorMessage );

                    FileInformation fileInfo = new FileInformation();
                    fileInfo.Url = url;
                    fileInfo.LocalPath = file.Path;

                    updateCache( fileInfo );
                }
            } );
        }

        /// <summary>
        /// Trying to find file in cache. If it doesn't exist it will be downloaded
        /// </summary>
        /// <param name="url">Full url to file</param>
        /// <param name="fileName">File will be saved with this name or file name will generate without extension</param>
        /// <returns>Storage file or null in error case</returns>
        public Task<StorageFile> GetOrDownloadFile( string url, string fileName = "" )
        {
            StorageFile file = GetStorageFile( url );

            if ( file != null )
                return Task.FromResult( file );

            return DownloadFile( url, fileName );
        }

        #region Cache

        private void updateCache( FileInformation fileInformation )
        {
            string url = fileInformation.Url;
            if ( fileCache.ContainsKey( url ) )
                fileCache[url] = fileInformation;
            else
                fileCache.Add( url, fileInformation );

            //save in DB
            dbConnection.InsertOrReplace( fileInformation );
        }

        #endregion
    }
}
