﻿using SQLite.Net.Attributes;

namespace FileCacheManager.Files
{
    [Table( "FileInformation" )]
    internal class FileInformation
    {
        [PrimaryKey]
        public string Url { get; set; }
        public string LocalPath { get; set; }

        public FileInformation()
        {
            Url = string.Empty;
            LocalPath = string.Empty;
        }

        internal FileInformation( string url, string localPath )
        {
            Url = url;
            LocalPath = localPath;
        }
    }
}
