﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Windows.Web.Http;
using Windows.Storage;
using Windows.Storage.Streams;

namespace FileCacheManager.Network
{
    public class FileDownloader
    {
        private const int MAX_PATH_LENGHT = 256;

        private HttpClient httpClient;

        internal FileDownloader()
        {
            httpClient = new HttpClient();

            //enable gzip
            httpClient.DefaultRequestHeaders.AcceptEncoding.Add( new Windows.Web.Http.Headers.HttpContentCodingWithQualityHeaderValue( "gzip" ) );
            httpClient.DefaultRequestHeaders.AcceptEncoding.Add( new Windows.Web.Http.Headers.HttpContentCodingWithQualityHeaderValue( "deflate" ) );
        }

        public void SetCookie( Dictionary<string, string> cookie )
        {
            foreach ( KeyValuePair<string, string> c in cookie )
            {
                try
                {
                    httpClient.DefaultRequestHeaders.Cookie.Add( new Windows.Web.Http.Headers.HttpCookiePairHeaderValue( c.Key, c.Value ) );
                }
                catch ( Exception e )
                {
                    System.Diagnostics.Debug.WriteLine( "FileDownloader: can't set cookie: {0}-{1}\n{2}", c.Key, c.Value, e.Message );
                }
            }
        }

        public void AddCustomHeader( string headerName, string headerValue )
        {
            lock ( httpClient )
            {
                if ( !httpClient.DefaultRequestHeaders.ContainsKey( headerName ) )
                    httpClient.DefaultRequestHeaders.Add( headerName, headerValue );
            }
        }

        internal StorageFile DownloadFile( string url, string fileName )
        {
            lock ( httpClient )
            {
                try
                {
                    Uri uri = new Uri( url );

                    HttpResponseMessage response = httpClient.GetAsync( uri ).AsTask().Result;

                    if ( response.StatusCode == HttpStatusCode.Ok )
                    {
                        return saveFileToSystemTemporaryFolder( fileName, getFileContent( response ) );
                    }

                    System.Diagnostics.Debug.WriteLine( "{0} {1}\n{2}", url, response.StatusCode, url );
                }
                catch ( Exception e )
                {
                    System.Diagnostics.Debug.WriteLine( "FileDownloader: URL: {0}; Exception: {1}", url, e.Message );
                }
            }

            return null;
        }

        private byte[] getFileContent( HttpResponseMessage response )
        {
            List<byte> fileContent = new List<byte>();

            using ( IInputStream readStream = response.Content.ReadAsInputStreamAsync().AsTask().Result )
            {
                using ( BinaryReader binaryReader = new BinaryReader( readStream.AsStreamForRead(), Encoding.UTF8 ) )
                {
                    //read file
                    int numBytesForRead = 1024;
                    while ( true )
                    {
                        byte[] readedBytes = binaryReader.ReadBytes( numBytesForRead );

                        fileContent.AddRange( readedBytes );

                        //-ER- all data is read
                        if ( readedBytes.Length < numBytesForRead )
                            break;
                    }
                }
            }

            return fileContent.ToArray();
        }

        private StorageFile saveFileToSystemTemporaryFolder( string fileName, byte[] fileContent )
        {
            StorageFolder storageFolder = ApplicationData.Current.TemporaryFolder;

            if ( string.IsNullOrEmpty( fileName ) )
                fileName = generateFileName();

            string pathToFile = Path.Combine( storageFolder.Path, fileName );
            if ( pathToFile.Length >= MAX_PATH_LENGHT )
            {
                string extenstion = Path.GetExtension( fileName );
                fileName = string.Format( "{0}{1}", generateFileName(), extenstion );
            }

            StorageFile file = storageFolder.CreateFileAsync( fileName, CreationCollisionOption.GenerateUniqueName ).AsTask().Result;

            using ( StorageStreamTransaction writeStream = file.OpenTransactedWriteAsync().AsTask().Result )
            {
                using ( DataWriter dateWriter = new DataWriter( writeStream.Stream ) )
                {
                    dateWriter.WriteBytes( fileContent );
                    dateWriter.StoreAsync().AsTask().Wait();
                    dateWriter.FlushAsync().AsTask().Wait();
                    writeStream.Stream.FlushAsync().AsTask().Wait();
                    writeStream.CommitAsync().AsTask().Wait();
                }
            }

            return file;
        }

        private string generateFileName()
        {
            return Guid.NewGuid().ToString().Replace( "-", "" );
        }
    }
}
