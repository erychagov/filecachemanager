# README #

### What is this repository for? ###

Download and save file in system temporary folder for future using

### How do I get set up? ###

1. Install package using NuGet manager

### Examples ###

* Creating and initialization:
```
#!c#
//simple variant
CacheManager fileManager = new CacheManager( "cache.db" );
await fileManager.Initialization();
```

* If your server demands special authorization, you can use callback:
```
#!c#
//variant with authorization
CacheManager fileManager = new CacheManager( "cache.db", authorizationCallback );

//Initialization will be after authorization
await fileManager.Initialization();
```

* Authorization function example:
```
#!c#
//authorization callback will be called from constructor
private void authorizationCallback(FileDownloader fileDownloader)
{
    //you can set cookie
    Dictionary<string,string> cookie = new Dictionary<string,string>();

    //your code

    fileDownloader.SetCookie( cookie );

    //also you can add custom headers if you need it
    string headerName = string.Empty;
    string headerValue = string.Empty;

    //your code

    fileDownloader.AddCustomHeader( headerName, headerValue );
}
```

* Download picture by url without special name:
```
#!c#
//storage file will be null in error case
StorageFile storageFile = await fileManager.DownloadFile( url );
```

* Save file with your name:
```
#!c#
//if file exists with same name then unique name will be generated for new file
StorageFile storageFile = await fileManager.DownloadFile( url, fileName );
```

* Get file from cache:
```
#!c#
//file will be null if you never downloaded this file
//so you can use this function to check cache
StorageFile file = fileManager.GetStorageFile( url );

```

* Add file to cache
```
#!c#
string url = string.Empty;
Storage file = null;

//your code

await fileManager.AddStorageFileToCache( url, file );
```

* Get or download file from cache
```
#!c#
string url = string.Empty;
string fileName = string.Empty;

//your code

StorageFile file = await fileManager.GetOrDownloadFile( url, fileName );
```

* Close connection
```
#!c#
//close connection to DB and clear cache in RAM
//you should call "Initialization" before using again
fileManager.Close();
```

* Clear cache
```
#!c#
//clear DB, remove all files, clear cache in RAM
//you can use object without reinitialization
await fileManager.ClearCache();
```